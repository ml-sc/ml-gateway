CREATE TABLE ml_predictors
(
    id             UUID    NOT NULL,
    data_set_name  VARCHAR NOT NULL,
    algorithm_name VARCHAR NOT NULL,
    topic          VARCHAR NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO ml_predictors(id, data_set_name, algorithm_name, topic)
VALUES ('123e4cdd-bb78-4769-a0c7-cb948a9f1238', 'Iris', 'Decision Tree', 'ml-predictor-iris-decision-tree'),
       ('223e4cdd-bb78-4769-a0c7-cb948a9f1238', 'Iris', 'SVC', 'ml-predictor-iris-svc'),
       ('323e4cdd-bb78-4769-a0c7-cb948a9f1238', 'Breast Cancer', 'Decision Tree', 'ml-predictor-breast-cancer-tree');
