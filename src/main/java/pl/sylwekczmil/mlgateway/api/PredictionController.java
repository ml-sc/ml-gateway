package pl.sylwekczmil.mlgateway.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.sylwekczmil.mlgateway.domain.prediction.PredictionService;
import pl.sylwekczmil.mlgateway.shared.dto.prediction.InitializePredictionDTO;

import java.util.Collections;
import java.util.Map;

@RequestMapping("/api/v1/predictions")
@RestController
@AllArgsConstructor
public class PredictionController {

    private final PredictionService predictionService;

    @PostMapping
    public Map<String, String> initializePrediction(@RequestBody InitializePredictionDTO request) throws JsonProcessingException {
        return Collections.singletonMap("predictionId", predictionService.initializePrediction(request));
    }

    @GetMapping("/{id}/result")
    public Map<String, Object> getPredictionResult(@PathVariable String id) {
        return predictionService.getPredictionResult(id);
    }
}
