package pl.sylwekczmil.mlgateway.api;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.sylwekczmil.mlgateway.domain.predictor.PredictorService;
import pl.sylwekczmil.mlgateway.shared.dto.predictor.PredictorDTO;

import java.util.List;

@RequestMapping("/api/v1/predictors")
@RestController
@AllArgsConstructor
public class PredictorController {

    private final PredictorService predictorService;

    @GetMapping
    public List<PredictorDTO> findAll() {
        return predictorService.findAll();
    }
}
