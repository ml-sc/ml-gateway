package pl.sylwekczmil.mlgateway.domain.prediction;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.Map;

@Document(collection = "ml-prediction")
@Data
public class Prediction {

    @Id
    private String id;
    private String user;
    private Map<String, Object> data;
    private Map<String, Object> result;
}
