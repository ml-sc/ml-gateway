package pl.sylwekczmil.mlgateway.domain.prediction;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


public interface PredictionRepository extends MongoRepository<Prediction, String> {


    default Prediction getOne(String id) {
        return findById(id).orElseThrow(() -> new PredictionNotFound(id));
    }


    @ResponseStatus(HttpStatus.NOT_FOUND)
    class PredictionNotFound extends RuntimeException {
        public PredictionNotFound(String id) {
            super(String.format("Prediction %s not found", id));
        }
    }
}
