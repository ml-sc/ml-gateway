package pl.sylwekczmil.mlgateway.domain.prediction;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sylwekczmil.mlgateway.domain.predictor.PredictorService;
import pl.sylwekczmil.mlgateway.shared.KafkaTopics;
import pl.sylwekczmil.mlgateway.shared.UserAccessor;
import pl.sylwekczmil.mlgateway.shared.dto.prediction.InitializePredictionDTO;
import pl.sylwekczmil.mlgateway.shared.dto.prediction.PredictionDTO;

import java.util.Map;

@Service
@Log4j2
@AllArgsConstructor
public class PredictionService {

    private final PredictorService predictorService;
    private final PredictionRepository predictionRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final ObjectMapper objectMapper;
    private final ModelMapper modelMapper;


    @KafkaListener(topics = KafkaTopics.RESULTS_TOPIC)
    void listenToPredictionResults(@Payload String payload) throws JsonProcessingException {
        log.info("Received: {}", payload);
        PredictionDTO predictionDTO = objectMapper.readValue(payload, PredictionDTO.class);
        savePrediction(predictionDTO);
    }


    public String initializePrediction(InitializePredictionDTO request) throws JsonProcessingException {
        var predictorId = request.getPredictorId();
        var topic = predictorService.getTopic(predictorId);
        var user = UserAccessor.getUsername();
        var dto = new PredictionDTO(user, predictorId, request.getData());
        kafkaTemplate.send(topic, objectMapper.writeValueAsString(dto));
        return dto.getId();
    }

    public void savePrediction(PredictionDTO dto) {
        Prediction prediction = modelMapper.map(dto, Prediction.class);
        predictionRepository.save(prediction);
        log.info("Saved: {}", prediction);
    }

    public Map<String, Object> getPredictionResult(String id) {
        return predictionRepository.findById(id).map(Prediction::getResult).orElseGet(
                () -> {
                    log.info("Result for prediction: {} not found", id);
                    return null;
                }
        );
    }
}
