package pl.sylwekczmil.mlgateway.domain.predictor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface PredictorRepository extends JpaRepository<Predictor, UUID> {
}
