package pl.sylwekczmil.mlgateway.domain.predictor;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import pl.sylwekczmil.mlgateway.shared.dto.predictor.PredictorDTO;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PredictorService {

    private final PredictorRepository predictorRepository;
    private final ModelMapper modelMapper;

    public List<PredictorDTO> findAll() {
        return predictorRepository.findAll().stream().map(
                x -> modelMapper.map(x, PredictorDTO.class)
        ).collect(Collectors.toList());
    }

    public String getTopic(String predictorId) {
        return predictorRepository.getOne(UUID.fromString(predictorId)).getTopic();
    }
}
