package pl.sylwekczmil.mlgateway.domain.predictor;

import lombok.Data;
import pl.sylwekczmil.mlgateway.domain.BaseEntity;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ml_predictors")
public class Predictor extends BaseEntity {

    private String dataSetName;
    private String algorithmName;
    private String topic;

}
