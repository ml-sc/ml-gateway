package pl.sylwekczmil.mlgateway.domain;


import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Data
@MappedSuperclass
public class BaseEntity {
    @Id
    private UUID id;

    public BaseEntity() {
        this.id = UUID.randomUUID();
    }
}
