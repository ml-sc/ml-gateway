package pl.sylwekczmil.mlgateway.shared.dto.prediction;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class InitializePredictionDTO {
    private String predictorId;
    private Map<String, Object> data;
}
