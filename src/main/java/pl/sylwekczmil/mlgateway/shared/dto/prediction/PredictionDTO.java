package pl.sylwekczmil.mlgateway.shared.dto.prediction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PredictionDTO {
    private String id = UUID.randomUUID().toString();
    private String user;
    private String predictorId;
    private Map<String, Object> data;
    private Map<String, Object> result;

    public PredictionDTO(String user, String predictorId, Map<String, Object> data) {
        this.user = user;
        this.predictorId = predictorId;
        this.data = data;
    }
}
