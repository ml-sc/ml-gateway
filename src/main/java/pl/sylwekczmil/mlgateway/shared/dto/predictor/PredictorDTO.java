package pl.sylwekczmil.mlgateway.shared.dto.predictor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PredictorDTO {

    private String id;
    private String dataSetName;
    private String algorithmName;

}
