package pl.sylwekczmil.mlgateway.shared;

import org.springframework.security.core.context.SecurityContextHolder;

public class UserAccessor {
    public static String getUsername() {
        try {
            return SecurityContextHolder.getContext().getAuthentication().getName();
        } catch (Exception e) {
            return "anonymousUser";
        }
    }
}
