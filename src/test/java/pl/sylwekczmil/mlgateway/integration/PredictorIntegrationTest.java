package pl.sylwekczmil.mlgateway.integration;

import org.junit.jupiter.api.Test;
import pl.sylwekczmil.mlgateway.shared.dto.predictor.PredictorDTO;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class PredictorIntegrationTest extends BaseIntegrationTest {

    @Test
    public void shouldReturnListOfPredictors() {
        // given
        String url = prepareUrl("predictors");

        // when
        PredictorDTO[] response = this.restTemplate.getForObject(url, PredictorDTO[].class);

        // then
        assertArrayEquals(
                new PredictorDTO[]{
                        new PredictorDTO("123e4cdd-bb78-4769-a0c7-cb948a9f1238", "Iris", "Decision Tree"),
                        new PredictorDTO("223e4cdd-bb78-4769-a0c7-cb948a9f1238", "Iris", "SVC"),
                        new PredictorDTO("323e4cdd-bb78-4769-a0c7-cb948a9f1238", "Breast Cancer", "Decision Tree")
                },
                response
        );
    }
}