package pl.sylwekczmil.mlgateway.integration;


import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import pl.sylwekczmil.mlgateway.domain.prediction.PredictionService;
import pl.sylwekczmil.mlgateway.domain.predictor.Predictor;
import pl.sylwekczmil.mlgateway.domain.predictor.PredictorRepository;
import pl.sylwekczmil.mlgateway.shared.dto.prediction.InitializePredictionDTO;
import pl.sylwekczmil.mlgateway.shared.dto.prediction.PredictionDTO;

import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Log4j2
public class PredictionIntegrationTest extends BaseIntegrationTest {

    @Autowired
    private PredictorRepository predictorRepository;

    @Autowired
    private PredictionService predictionService;

    @Test
    public void shouldInitializePrediction() throws JsonProcessingException {

        // given
        Predictor predictor = predictorRepository.findAll().get(0);
        log.info("Initialize prediction");
        var initializeRequest = InitializePredictionDTO.builder()
                .predictorId(predictor.getId().toString())
                .data(Collections.singletonMap("values", new Integer[]{1, 1, 1, 1}))
                .build();

        // when
        var initializeResponse = this.restTemplate.postForObject(prepareUrl("predictions"), initializeRequest, Map.class);
        var predictionId = String.valueOf(initializeResponse.get("predictionId"));
        var value = objectMapper.readValue(
                KafkaTestUtils.getRecords(kafkaConsumer).records(predictor.getTopic()).iterator().next().value().toString(),
                PredictionDTO.class
        );

        // then
        assertEquals(predictionId, value.getId());
    }


    @Test
    public void shouldRetrievePredictionResult() throws JsonProcessingException {

        // given
        var value = new PredictionDTO();
        value.setResult(Collections.singletonMap("prediction", "[0]"));
        predictionService.savePrediction(value);

        // when
        var resultsUrl = prepareUrl(String.format("predictions/%s/result", value.getId()));
        var resultsResponse = this.restTemplate.getForObject(resultsUrl, Map.class);

        // then
        assertNotNull(resultsResponse);
        assertEquals("[0]", resultsResponse.get("prediction"));
    }
}