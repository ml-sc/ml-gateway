package pl.sylwekczmil.mlgateway.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.producer.Producer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.regex.Pattern;

@EmbeddedKafka(partitions = 1)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BaseIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    EmbeddedKafkaBroker embeddedKafka;

    Consumer<Object, Object> kafkaConsumer;
    Producer<Object, Object> kafkaProducer;

    @BeforeEach
    public void init() {
        kafkaConsumer = createKafkaConsumer(embeddedKafka);
        kafkaProducer = createKafkaProducer(embeddedKafka);
    }

    String prepareUrl(String endpoint) {
        return "http://localhost:" + port + "/api/v1/" + endpoint;
    }

    Consumer<Object, Object> createKafkaConsumer(EmbeddedKafkaBroker embeddedKafka) {
        var consumerProps = KafkaTestUtils.consumerProps("testGroup", "true", embeddedKafka);
        var cf = new DefaultKafkaConsumerFactory<>(
                consumerProps);
        Consumer<Object, Object> consumer = cf.createConsumer();
        consumer.subscribe(Pattern.compile("(.*?)"));
        return consumer;
    }

    Producer<Object, Object> createKafkaProducer(EmbeddedKafkaBroker embeddedKafka) {
        var producerProps = KafkaTestUtils.producerProps(embeddedKafka);
        var pf = new DefaultKafkaProducerFactory<>(producerProps);
        return pf.createProducer();
    }

}