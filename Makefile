.PHONY: build deploy_dev destroy_dev deploy destroy

ML_DATA_DIR?=/srv/ml
ML_POSTGRES_PASSWORD?=pgPass123
ML_MONGO_PASSWORD?=mgPass123

build:
	@./mvnw clean install

.env:
	@touch .env
	@echo "ML_DATA_DIR=$(ML_DATA_DIR)" >> .env
	@echo "ML_POSTGRES_PASSWORD=$(ML_POSTGRES_PASSWORD)" >> .env
	@echo "ML_MONGO_PASSWORD=$(ML_MONGO_PASSWORD)" >> .env

deploy_dev:
	@docker-compose -f docker-compose-dev.yml -p "ml-gateway-dev" up -d

destroy_dev:
	@docker-compose -f docker-compose-dev.yml -p "ml-gateway-dev" down

deploy: .env
	@docker-compose -f docker-compose.yml -p "ml-gateway" up --build -d

destroy: .env
	@docker-compose -f docker-compose.yml -p "ml-gateway" down
	@rm -rf .env $(ML_DATA_DIR) || true
